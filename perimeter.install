<?php

/**
 * @file
 * Installation hooks for the perimeter module.
 */

/**
 * Add not_found_exception_patterns on update if not yet set.
 */
function perimeter_update_8201() {
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('perimeter.settings');
  $not_found_exception_patterns = $config->get('not_found_exception_patterns');
  if (empty($not_found_exception_patterns)) {
    // Set if empty (not set yet):
    $config->set('not_found_exception_patterns', [
      '/.*\\.aspx/',
      '/.*\\.asp/',
      '/.*\\.jsp/',
      '/\\/blog_edit\\.php/',
      '/\\/blogs\\.php/',
      '/\\/wp-admin.*/',
      '/\\/wp-login.*/',
      '/\\/my_blogs/',
      '/\\/system\\/.*\\.php/',
      '/.*systopice.*/',
      '/.*login.json/',
      '/\\/episerver.*/',
    ]);
    $config->save(TRUE);
  }
}

/**
 * Initiate newly added "whitelisted_ips" config and update user permissions.
 */
function perimeter_update_8202() {
  // Add new config:
  $config_factory = \Drupal::configFactory();
  $config = $config_factory->getEditable('perimeter.settings');
  $config->set('whitelisted_ips', [])->save(TRUE);
  // Update user permissions:
  $user_roles = \Drupal::entityTypeManager()->getStorage('user_role')->loadMultiple();
  foreach ($user_roles as $user_role) {
    if ($user_role->hasPermission('administer site configuration')) {
      user_role_grant_permissions($user_role->id(), [
        'administer perimeter url patterns',
        'administer perimeter ip whitelist',
      ]);
    }
  }
}

/**
 * Introducing flood_threshold and flood_window setting default values.
 */
function perimeter_update_10001() {
  \Drupal::configFactory()->getEditable('perimeter.settings')
    ->set('flood_threshold', 0)
    ->set('flood_window', 3600)
    ->save();
}
