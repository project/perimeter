<?php

namespace Drupal\Tests\perimeter\Functional;

use Drupal\Tests\system\Functional\Module\GenericModuleTestBase;

/**
 * Generic module tests for the perimeter module.
 *
 * @group perimeter
 */
class GenericTest extends GenericModuleTestBase {}
